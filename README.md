# Canuck Credit Card Comparison

Join me in finding the ideal credit card(s) for the north. Just starting now, will check back every few days/weeks to properly update this list with *good* cards.  

|	#	|	Service	|	Card	|	Bank	|	Travel ins.	|	Other ins.	|	Points/cash-back	|	Fee	|	Qualification	|
|	:-:	|	:-	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	
|	1	|	[WestJet World Elite](https://www.rbcroyalbank.com/credit-cards/travel/westjet-rbc-world-elite-mastercard.html)	|	Mastercard	|	RBC	|		|		|		|	$119 CAD	|		
|	2	|	[True Line](https://www.greedyrates.ca/blog/best-0-balance-transfer-credit-card-offers-canada/#trueline)	|	Mastercard	|	MBNA	|		|		|		|	$0	|		
|	3	|	[True Line Gold](https://www.greedyrates.ca/blog/best-0-balance-transfer-credit-card-offers-canada/#truelinegold)	|	Mastercard	|	MBNA	|		|		|		|	$39 CAD	|		
